window.onload = function() {
    window.mediaElement = document.getElementById('media');
    window.mediaManager = new cast.receiver.MediaManager(window.mediaElement);
    window.castReceiverManager = cast.receiver.CastReceiverManager.getInstance();
    var bus = window.castReceiverManager.getCastMessageBus('urn:x-cast:maestro');
    bus.onMessage = onMessage;

    window.castReceiverManager.start();
}

var rootDirectoryID;
var pathFromRoot;
var files;
var video = document.getElementById("media");
var serverUrl = "";

window.addEventListener("message", function(event) {
    console.log(event);
});

function getValidServerUrl(serverUrls, callback) {
    var currentServerUrl = serverUrls[0];
    $.ajax({
        url: currentServerUrl+"/api/Health",
        success: function() {
            window.serverUrl = currentServerUrl;
            callback();
        }, error: function() {
            serverUrls.splice(0,1);
            getValidServerUrl(serverUrls,callback);
        }
    })
}

function onMessage(event) {
    var message = JSON.parse(event.data);
    console.log("message in");
    console.log(message);
    console.log(message.type);
    if (message.type == "play") {
        getValidServerUrl(message.serverUrls, function() {
            rootDirectoryID = message.r;
            pathFromRoot = message.p;
            console.log("loading season");
            loadSeason(function(files) {
                window.files = files;
                playVideo(message.i);
            });
        });

    }

}

function playVideo(index) {
    $(video).empty();
    window.index = index;
    var src = "/api/videos";
    var params = new Object();
    params.rootDirectoryID = rootDirectoryID;
    if (typeof pathFromRoot != "undefined" && pathFromRoot != "") {
        params.pathFromRoot = pathFromRoot + "/";
    }
    params.name = window.files[index].name;
    src += "?" + $.param(params, true);

    src = window.serverUrl + src;

    var source = document.createElement("source");
    $(source).attr("type", "video/mp4");
    $(source).attr("src", src);
    $(video).append(source);

    video.play();
}

function playNextVideo() {
    playVideo(window.index + 1);
}

function loadSeason(callback) {
    $.ajax({
        url: serverUrl+"/api/FolderList",
        type: 'GET',
        data: {
            rootDirectoryID: rootDirectoryID,
            pathFromRoot: pathFromRoot,
            name: ""
        },
        success: function(response) {
            console.log(response);
            data = response.data;
            callback(data.files);
        }, error: function(err) {
                console.log(err);
        }
    });
}

$(video).bind('ended', function() {
    playNextVideo();
});
