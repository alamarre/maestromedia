Maestro.MenuItemManager=function() {
    this.items=new Array();
    this.display=document.createElement("div");
    this.activeIndex=0;
    this.activeItem=null;
}

Maestro.MenuItemManager.prototype.addItem=function(item) {
    if(this.items.length==0) {
        $(item.getDisplay()).addClass("activeItem");
        this.activeItem = item;
    }
    this.items.push(item);
    this.display.appendChild(item.getDisplay());
    if(item.windowToOpen!=null) {
        Maestro.windowManager.addWindow(item.windowToOpen);
    }
}

Maestro.MenuItemManager.prototype.highlightCurrentItem=function() {
    if(this.activeItem!=null) {
        $(this.activeItem.getDisplay()).removeClass("activeItem");
    }
    $(this.items[this.activeIndex].getDisplay()).addClass("activeItem");
    this.activeItem = this.items[this.activeIndex];
}

Maestro.MenuItemManager.prototype.goToPreviousItem=function() {
    this.activeIndex = this.activeIndex-1;
    if(this.activeIndex<0) {
        this.activeIndex=this.items.length-1;
    }
    this.highlightCurrentItem();
}

Maestro.MenuItemManager.prototype.goToNextItem=function() {
    this.activeIndex= this.activeIndex+1;
    if(this.activeIndex>=this.items.length) {
        this.activeIndex=0;
    }
    this.highlightCurrentItem();
}

Maestro.MenuItemManager.prototype.scrollToActive=function() {
    $.scrollTo($(this.activeItem.getDisplay()).offset().top);
}

Maestro.MenuItemManager.prototype.enter=function() {
    if(this.activeItem!=null) {
        $(this.activeItem.getDisplay()).click()
    }
}

Maestro.MenuItemManager.prototype.clear=function() {
    this.activeIndex=0;
    this.activeItem=null;
    this.items=new Array();
    $(this.display).html("");
}

Maestro.MenuItemManager.prototype.getDisplay=function() {
    return this.display;
}

Maestro.MenuItemManager.prototype.sort=function(sortFunction) {
    for(var i=0; i<this.items.length; i++) {
        this.display.removeChild(this.items[i].getDisplay());
    }
    if(typeof sortFunction=="undefined") {
        sortFunction=Maestro.alphabeticItemSort;
    }
    this.items.sort(sortFunction);
    for(var i=0; i<this.items.length; i++) {
        this.display.appendChild(this.items[i].getDisplay());
    }
}

Maestro.alphabeticItemSort=function(a,b) { 
    return Maestro.stringCompare(a.name,b.name);
}

Maestro.stringCompare=function(a,b) {
    if(a.toString()<b.toString()) {
        return -1;
    }
    if(a.toString()>b.toString()) {
        return 1;
    }
    return 0;
}



