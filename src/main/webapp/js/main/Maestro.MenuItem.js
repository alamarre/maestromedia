Maestro.MenuItem=function() {
    this.name="Name";
    this.windowToOpen=null;
}

Maestro.MenuItem.prototype.createDisplay=function() {
    this.display=document.createElement("div");
    $(this.display).addClass("menuItem");
    var self=this;
    this.display.onclick=function() {
        self.click();
    }
    $(this.display).html(this.name);
}

Maestro.MenuItem.prototype.getName=function() {
    return this.name;
}

Maestro.MenuItem.prototype.getDisplay=function() {
    return this.display;
}

Maestro.MenuItem.prototype.click=function() {
    if(this.windowToOpen!=null) {
        Maestro.windowManager.switchTo(this.windowToOpen);
    }
}

Maestro.MenuItem.prototype.highlight=function() {
    
    }
