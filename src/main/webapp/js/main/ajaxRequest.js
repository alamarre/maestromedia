function ajaxRequest(request) {
    var requestInfo ={
        url: rootDirectory+request.url,
        type: "POST",
        success: function(data) {
                if(typeof data =="string") {
                    data=JSON.parse(data);
                }
                Maestro.checkForLogin(data);
                if(data['result']=="success") {
                    if(typeof request.success=="function") {
                        
                        request.success(data);
                        
                    }
                } else if (data.message) {
                    alert(data.message);
                }
            
        }, 
        error: function(jqXHR,t,e) {
            
            //alert("error "+t+" exception "+e);
            //alert(jqXHR.status+" "+jqXHR.responseText);
            if(typeof request.error=="function") {
                try {
                    request.error(data);
                } catch(error) {
                //console.log(e.stack);
                }
            } else {
            //alert("An error has occurred");
            }
        }
    };
    
    if(typeof request.data !="undefined") {
        requestInfo.data=request.data;
    }
    if(typeof request.type !="undefined") {
        requestInfo.type=request.type;
    }
    $.ajax(requestInfo);
}
