function Maestro() {}

Maestro.WindowManager = function() {
    this.currentWindow = null;
    this.element=null;
    this.stack = new Array();
}

Maestro.WindowManager.prototype.addWindow=function(window) {
    if(this.element!=null) {
        $(this.element).append(window.getDisplay());
    }
    if(this.currentWindow!=null) {
        $(window.getDisplay()).hide();
    }
}

Maestro.WindowManager.prototype.switchTo=function(window) {
    if(this.currentWindow!=null) {
        this.stack.push(this.currentWindow);
        $(this.currentWindow.getDisplay()).hide();
    }
    this.currentWindow=window;
    if(this.currentWindow.activate) {
       this.currentWindow.activate(); 
    }
    $(this.currentWindow.getDisplay()).show();
}

Maestro.WindowManager.prototype.back=function() {
    if(this.stack.length>0) {
        if(typeof this.currentWindow.back=="function") {
            if(this.currentWindow.back()) {
                return;
            }
        }
        $(this.currentWindow.getDisplay()).hide();
        this.currentWindow=this.stack.splice(this.stack.length-1)[0];
        
        $(this.currentWindow.getDisplay()).show();
    }
}

