var overlay=document.createElement("div");
$(overlay).addClass("overlayCover");

$(overlay).hide();
$('body').append(overlay);
$('body').keydown(function(e) {
    console.log(e.keyCode);
    if(e.keyCode==13) {
        if(Maestro.windowManager.currentWindow.enter) {
            Maestro.windowManager.currentWindow.enter();
        }
    }
    if(e.keyCode==38) {
        e.preventDefault();
        if(Maestro.windowManager.currentWindow.goUp) {
            Maestro.windowManager.currentWindow.goUp();
        }
    }
    if(e.keyCode==40) {
        e.preventDefault();
        if(Maestro.windowManager.currentWindow.goDown) {
            Maestro.windowManager.currentWindow.goDown();
        }
    }
    if(e.keyCode==98) {
        $(overlay).toggle();
    }
    if(e.keyCode==84) {
        $(overlay).toggle();
    }
    if(e.keyCode==46) {
        Maestro.windowManager.back();
    }
    
    console.log(e.keyCode);
    if(typeof Maestro.player!="undefined") {
        if(e.keyCode==22650||e.keyCode==78) {
            Maestro.player.playNext();
        } else if(e.keyCode==22651||e.keyCode==80) {
            Maestro.player.playPrevious();
        } if(e.keyCode==32) {
            Maestro.player.continuePlayback();
        }
        if(e.keyCode==85) {
            Maestro.player.pause();
        }
        if(e.keyCode==82) {
            Maestro.player.resize();
        }
    }
});

