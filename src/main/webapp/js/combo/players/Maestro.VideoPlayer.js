Maestro.VideoPlayer = function() {
    this.display = document.createElement("div");
    $(this.display).addClass("videoContainer");
    this.video = null;
    this.source = null;
    this.playList = null;
    this.index = 0;
    this.swapSources = true;
    this.session = null;

    this.receivers = new Array();
    this.chromecastApplicationId = "7B866935";
    var player = this;
    this.initializeChromeCast();
    
    if(Maestro.getQueryStringParameter("chromecast")) {
        var castButton = document.createElement("button");
        $(document.body).append(castButton);
        $(castButton).html("Launch");
        $(castButton).css("position","absolute");
        $(castButton).css("bottom","0");
        $(castButton).css("right","0");
        $(castButton).click(function() {
            player.launchChromeCast();
        })
    }
}

Maestro.VideoPlayer.prototype.initializeChromeCast = function() {
    if(typeof chrome=="undefined") {
        return;
    }
    if (!chrome.cast || !chrome.cast.isAvailable) {
        setTimeout(this.initializeChromeCast.bind(this), 1000);
        return;
    }
    var sessionRequest = new chrome.cast.SessionRequest(this.chromecastApplicationId);
    var apiConfig = new chrome.cast.ApiConfig(sessionRequest,
            this.sessionListener.bind(this),
            this.receiverListener.bind(this));
    chrome.cast.initialize(apiConfig, this.onCastInitSuccess.bind(this), this.onCastInitError.bind(this));
}

Maestro.VideoPlayer.prototype.launchChromeCast = function() {
    chrome.cast.requestSession(this.onRequestSessionSuccess.bind(this), this.onLaunchError.bind(this));
}

Maestro.VideoPlayer.prototype.onRequestSessionSuccess = function() {
    console.log("session success");
}

Maestro.VideoPlayer.prototype.onLaunchError = function() {
    console.log("launch failed");
}

Maestro.VideoPlayer.prototype.sessionListener = function(e) {
    this.session = e;
    if (this.session) {
        this.session.addUpdateListener(this.sessionUpdateListener.bind(this));
    }
}

Maestro.VideoPlayer.prototype.sessionUpdateListener = function(isAlive) {
    console.log("session " + isAlive);
    if(!isAlive) {
        this.session = false;
    }
}

/**
 * @param {string} e Receiver availability
 * This indicates availability of receivers but
 * does not provide a list of device IDs
 */
Maestro.VideoPlayer.prototype.receiverListener = function(e) {
    if (e === 'available') {
        console.log("receiver found");
    }
    else {
        console.log("receiver list empty");
    }
};


Maestro.VideoPlayer.prototype.onCastInitSuccess = function() {
    
}

Maestro.VideoPlayer.prototype.onCastRequestSessionSuccess = function() {
    console.log("launched chromecast session");
}

Maestro.VideoPlayer.prototype.onCastLaunchError = function() {
    console.log("failed to launch chromecast");
}

Maestro.VideoPlayer.prototype.onCastInitError = function() {
    console.log("failed to cast");
}

Maestro.VideoPlayer.prototype.setSource = function(source) {
    this.source = source;
}

Maestro.VideoPlayer.prototype.continuePlayback = function() {
    this.video.play();
}

Maestro.VideoPlayer.prototype.pause = function() {
    this.video.pause();
}


Maestro.VideoPlayer.prototype.play = function(index) {

    if (this.session) {
        this.playToChromeCast(index);
        return;
    }
    this.index = index;
    if (this.swapSources && this.video != null) {
        var video = this.video;
        video.addEventListener('loadeddata', function() {
            video.play();
        }, false);
        $(video).empty();
    } else {
        var video = document.createElement("video");
        $(video).attr("controls", "controls");
        $(video).attr("autoplay", "autoplay");
        $(video).attr("volume", 0.20);
    }

    if (index < 0) {
        index = 0;
    }
    if (this.playList == null) {
        return;
    }
    if (this.playList.length <= index) {
        var self = this;
        this.source.getNextList(function(files) {
            self.setPlayList(files);
            self.play(0);
        });
        return;
    }
    var file = this.playList[index];
    //$(video).attr("width",document.width);

    //$(video).attr("height",height);
    var src = rootDirectory + "/api/videos";
    var params = new Object();
    params.rootDirectoryID = file.rootDirectoryID;
    if (typeof file.pathFromRoot != "undefined" && file.pathFromRoot != "") {
        params.pathFromRoot = file.pathFromRoot + "/";
    }
    params.name = file.name;
    src += "?" + $.param(params, true);
    var source = document.createElement("source");
    $(source).attr("type", "video/mp4");
    $(source).attr("src", src);
    $(video).append(source);

    var current = this.video;
    if (current == null || !this.swapSources) {
        //current.stop();
        if (!this.swapSources) {
            $(current).remove();
        }
        this.display.appendChild(video);
        this.video = video;

        var self = this;
        $(video).bind('ended', function() {
            console.log("playing " + (self.index + 1) + " ");
            console.log(self.playlist);
            self.playNext();
        });

        var player = this;
        $(video).resize(function() {
            player.adjustHeight(video);
        });
        $(video).bind('loadedmetadata', function() {
            player.resize(this);
            this.play();
        });

        $(window).resize(function() {
            player.resize(video);
        });

        if (typeof video.play != "undefined") {
            //video.play();
        }
    }
    if (typeof video.load != "undefined") {
        video.load();
    }
}

Maestro.VideoPlayer.prototype.playToChromeCast = function(index) {

    this.index = index;

    if (index < 0) {
        index = 0;
    }
    var rootDirectoryID = this.source.rootDirectoryID;
    var pathFromRoot = this.source.pathFromRoot;

    var playMessage = {"type": "play", "r": rootDirectoryID, "p": pathFromRoot, "i": index};
    if (this.playList == null) {
        return;
    }
    if (this.playList.length <= index) {
        var self = this;
        this.source.getNextList(function(files) {
            self.setPlayList(files);
            self.playToChromeCast(0);
        });
        return;
    }
    var file = this.playList[index];
    //$(video).attr("width",document.width);

    //$(video).attr("height",height);
    var src = rootDirectory + "/api/videos";
    var params = new Object();
    params.rootDirectoryID = file.rootDirectoryID;
    if (typeof file.pathFromRoot != "undefined" && file.pathFromRoot != "") {
        params.pathFromRoot = file.pathFromRoot + "/";
    }
    params.name = file.name;
    src += "?" + $.param(params, true);

    src = window.location.origin + src;

    var either = function(result) {
        console.log(result);
    };
    
    playMessage.serverUrls = Maestro.serverUrls;
    
    if(this.session) {
        this.session.sendMessage('urn:x-cast:maestro', playMessage, either, either);
    }


}

Maestro.VideoPlayer.prototype.adjustHeight = function(video) {
    var screenHeight = window.innerHeight;
    if (!screenHeight) {
        screenHeight = document.documentElement.clientHeight;
    }
    var videoHeight = $("video").height();
    if (videoHeight < screenHeight) {
        var diff = screenHeight - videoHeight;
        console.log("setting top " + diff / 2);
        $(video).css("top", diff / 2);
    }
}


Maestro.VideoPlayer.prototype.resize = function(video) {
    $(video).css("top", 0);
    if (typeof video == "undefined") {
        video = this.video;
    }
    var width = video.videoWidth;
    var height = video.videoHeight;

    var screenHeight = window.innerHeight;
    var screenWidth = window.innerWidth;
    if (!screenHeight) {
        screenHeight = document.documentElement.clientHeight;
        screenWidth = document.documentElement.clientWidth;
    }

    var widthAdjustment = 0;
    var heightAdjustment = 0;

    $(video).attr("height", null);
    $(video).attr("width", null);
    if (height / screenHeight > width / screenWidth) {
        $(video).attr("height", screenHeight + heightAdjustment);
    } else {
        $(video).attr("width", screenWidth + widthAdjustment);
    }
    this.adjustHeight(video);
}

Maestro.VideoPlayer.prototype.setPlayList = function(files) {
    this.playList = files;
}

Maestro.VideoPlayer.prototype.playNext = function() {
    this.play(this.index + 1);
}

Maestro.VideoPlayer.prototype.playPrevious = function() {
    this.play(this.index - 1);
}

Maestro.VideoPlayer.prototype.getDisplay = function() {
    return this.display;
}


