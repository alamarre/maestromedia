Maestro.serverUrls = null;
Maestro.rootPath = window.location.pathname;
if(Maestro.rootPath=="/") {
    Maestro.rootPath="";
}

function updateServerUrls() {
    ajaxRequest({
        url: "/api/ServerIps",
        type: "GET",
        success: function(result) {
            var ips = result.data;
            var urls = new Array();
            urls.push("http://192.168.0.99");
            for(var i=0; i<ips.length; i++) {
                var url = window.location.protocol +"//"+ips[i]+":"+window.location.port+Maestro.rootPath;
                urls.push(url);
            }
            Maestro.serverUrls = urls;
        }
    })
}

if(Maestro.serverUrls==null) {
    Maestro.serverUrls = [window.location.origin];
    setInterval(updateServerUrls, 1000*60);
    updateServerUrls();
}