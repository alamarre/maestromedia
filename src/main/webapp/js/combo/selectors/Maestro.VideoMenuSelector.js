Maestro.VideoMenuSelector=function() {
    this.menuManager=new Maestro.MenuItemManager();
    this.display=document.createElement("div");
    this.display.appendChild(this.menuManager.getDisplay());
    this.items=new Array();
    this.addMenuItem(new Maestro.VideoMenuFolderItem());
}

Maestro.VideoMenuSelector.prototype.navigate=function(components, index) {
    for(var i=0; i<this.items.length; i++) {
        if(this.items[i].getName()==components[index]) {
            if(Maestro.currentHashSize<index) {
                Maestro.windowManager.switchTo(this.items[i].windowToOpen);
            }
            this.items[i].windowToOpen.navigate(components, index+1);
        }
    }
}

Maestro.VideoMenuSelector.prototype.enter=function() {
    this.menuManager.enter();
}

Maestro.VideoMenuSelector.prototype.activate=function() {
    window.location.hash="Videos";
}

Maestro.VideoMenuSelector.prototype.addMenuItem=function(item) {
    this.menuManager.addItem(item);
    this.items.push(item);
}
Maestro.VideoMenuSelector.prototype.getDisplay=function() {
    return this.display;
}


