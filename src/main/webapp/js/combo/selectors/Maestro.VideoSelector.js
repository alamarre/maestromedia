Maestro.VideoSelector=function() {
    this.menuManager=new Maestro.MenuItemManager();
    this.display=document.createElement("div");
    this.display.appendChild(this.menuManager.getDisplay());
    this.name="";
    this.pathFromRoot="";
    this.rootDirectoryID=0;
    this.folderStack = new Array();
    this.files=null;
    this.player=new Maestro.VideoPlayer();
    this.getRootList();
    this.videos=new Object();
    Maestro.windowManager.addWindow(this.player);
}

Maestro.VideoSelector.prototype.addMenuItem=function(item) {
    this.menuManager.addItem(item);
}

Maestro.VideoSelector.prototype.getDisplay=function() {
    return this.display;
}

Maestro.VideoSelector.prototype.setRootDirectoryID=function(rootDirectoryID) {
    this.rootDirectoryID=rootDirectoryID;
}

Maestro.VideoSelector.prototype.setName=function(name) {
    if(name!="") {
        window.location.hash = window.location.hash+"/"+name;
    }
    this.name=name;
}

Maestro.VideoSelector.prototype.setPathFromRoot=function(pathFromRoot) {
    if(pathFromRoot!="") {
        this.folderStack.push(pathFromRoot);
    }
    this.pathFromRoot=pathFromRoot;
}

Maestro.VideoSelector.prototype.back=function() {
    if(this.folderStack.length>0) {
        this.pathFromRoot=this.folderStack.splice(this.folderStack.length-1)[0];
        this.name="";
        this.updateList();
        
        return true;
    }
    if(this.pathFromRoot!="") {
        this.pathFromRoot="";
        this.name="";
        this.updateList();
        
        return true;
    }
    if(this.rootDirectoryID!=0) {
        this.rootDirectoryID=0;
        this.getRootList();
        return true;
    }
    return false;
}


Maestro.VideoSelector.prototype.setRootDirectoryID=function(rootDirectoryID) {
    if(this.rootDirectoryID==0) {
        Maestro.updateHash=false;
        window.location.hash = window.location.hash+"/"+rootDirectoryID;
        Maestro.updateHash=true;
    }
    this.rootDirectoryID=rootDirectoryID;
}

Maestro.VideoSelector.prototype.updateList=function() {
    var self=this;
    ajaxRequest({
        url: "/api/FolderList",
        type: 'GET',
        data: {
            rootDirectoryID: self.rootDirectoryID,
            pathFromRoot: self.pathFromRoot,
            name: this.name
        },
        success: function(response) {
            self.menuManager.clear();
            data=response.data;
            for(var i=0; i<data.folders.length; i++) {
                var current=data.folders[i];
                var createdItem = new Maestro.VideoFolderItem(current.name,self,current.rootDirectoryID,current.pathFromRoot,current.name);
                self.menuManager.addItem(createdItem);
            }
            //self.sortFiles(data.files);
            for(var i=0; i<data.files.length; i++) {
               
                current=data.files[i];
                var file = new Maestro.VideoItem(current.name,self,current.rootDirectoryID,current.pathFromRoot,i);
                self.menuManager.addItem(file);
                self.videos[current.name]=file;
            }
            self.files=data.files;
            //self.sort();
            //self.menuManager.sort();
            self.menuManager.highlightCurrentItem();
        }
    });
}

Maestro.VideoSelector.prototype.enter=function() {
    this.menuManager.enter();
}

Maestro.VideoSelector.prototype.goDown=function() {
    this.menuManager.goToNextItem();
    this.menuManager.scrollToActive();
}

Maestro.VideoSelector.prototype.goUp=function() {
    this.menuManager.goToPreviousItem();
    this.menuManager.scrollToActive();
}

Maestro.VideoSelector.prototype.sortFiles=function(files,sortFunction) {
    files.sort(sortFunction);
}

Maestro.VideoSelector.prototype.navigate=function(components, index) {
 
    if(Maestro.currentHashSize<index&&components.length>index) {
        
        window.location.hash = window.location.hash+"/"+components[index];
        Maestro.updateHash=false;
        this.rootDirectoryID = components[index];
        var pathFromRoot="";
        for(var i=index+1; i<components.length-1; i++) {
            this.setPathFromRoot(pathFromRoot);
            pathFromRoot+=components[i];
            
            window.location.hash = window.location.hash+"/"+components[i];
        }
        this.setPathFromRoot(pathFromRoot);
        this.name="";
        if(components.length-1>index) {
            this.name=components[components.length-1];
            window.location.hash = window.location.hash+"/"+this.name;
        }
        this.updateList();
        Maestro.updateHash=true;
        
    }
    this.menuManager.currentIndex=0;
    this.menuManager.highlightCurrentItem();
    
    
}

Maestro.VideoSelector.prototype.activate=function() {
    window.location.hash=window.location.hash+"/Folders";
}


Maestro.VideoSelector.prototype.sort=function(sortFunction) {
    if(typeof sortFunction=="undefined") {
        sortFunction=Maestro.alphabeticItemSort;
    }
    this.files.sort(sortFunction);
    for(var i=0; i<this.files.length; i++) {
        if(typeof this.videos[this.files[i].name]!="undefined") {
            this.videos[this.files[i].name].setIndex(i);
        }
    }

}

Maestro.VideoSelector.prototype.getRootList=function() {
    var self=this;
    ajaxRequest({
        url: "/api/RootFolders",
        type: 'GET',
        success: function(response) {
            if(self.pathFromRoot=="") {
                self.menuManager.clear();
                data=response.data;
                for(var i=0; i<data.length; i++) {
                    var createdItem = new Maestro.VideoFolderItem(data[i].name,self,data[i].rootDirectoryID,"","");
                    self.menuManager.addItem(createdItem);
                }
            }
        }
    });
}

Maestro.VideoSelector.prototype.play=function(index) {
    if(this.files!=null) {
        this.player.setPlayList(this.files);
        this.player.setSource(this);
        this.player.play(index);
        Maestro.player=this.player;
        
        Maestro.windowManager.switchTo(this.player);
    }
}

//files are returned as a parameter to the function provided
Maestro.VideoSelector.prototype.getNextList = function(callback) {
    var pathFromRoot = this.pathFromRoot;
    var dir=pathFromRoot.split("/");
    var parts=dir[dir.length-1].split(" ");
    var num = new Number(parts[parts.length-1]);
    if(num.toString()!="NaN") {
        num = num.valueOf();
        num++;
        parts[parts.length-1]=num;
        var name=parts.join(" ");
        dir.splice(-1,1);
        pathFromRoot=dir.join("/");
        var self = this;
        this.getFiles(pathFromRoot,name,function(files) {
            callback(files);
            var endPathFromRoot = pathFromRoot+"/"+name;
            self.pathFromRoot = endPathFromRoot;
        });
    }
}

Maestro.VideoSelector.prototype.getFiles = function (pathFromRoot, name, callback) {
    var self=this;
    ajaxRequest({
        url: "/api/FolderList",
        type: 'GET',
        data: {
            rootDirectoryID: self.rootDirectoryID,
            pathFromRoot: pathFromRoot,
            name: name
        },
        success: function(response) {
            data=response.data;
            var folders = new Array();
            for(var i=0; i<data.folders.length; i++) {
                var current=data.folders[i];
                var createdItem = new Maestro.VideoFolderItem(current.name,self,current.rootDirectoryID,current.pathFromRoot,current.name);
                folders[i]=createdItem;
            }
            //self.sortFiles(data.files);
            for(var i=0; i<data.files.length; i++) {
               
                current=data.files[i];
                var file = new Maestro.VideoItem(current.name,self,current.rootDirectoryID,current.pathFromRoot,i);
            }
            sortFunction=Maestro.alphabeticItemSort;
            //data.files.sort(sortFunction);
            console.log(data.files);
            callback(data.files);
        }
    });

}



