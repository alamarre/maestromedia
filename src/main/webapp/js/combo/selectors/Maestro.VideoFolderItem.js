Maestro.VideoFolderItem=function(name,selector,id,pathFromRoot,fileName) {
    this.name=name;
    this.createDisplay();
    this.id=id;
    this.pathFromRoot=pathFromRoot;
    this.display.onclick=function() {
        selector.setRootDirectoryID(id);
        selector.setName(fileName);
        selector.setPathFromRoot(pathFromRoot);
        selector.updateList();
    }
}

Maestro.VideoFolderItem.prototype=new Maestro.MenuItem();
Maestro.VideoFolderItem.constructor=Maestro.VideoFolderItem;