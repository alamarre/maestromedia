Maestro.VideoItem=function(name,selector,rootDirectoryID,pathFromRoot,index) {
    this.name=name;
    this.rootDirectoryID=rootDirectoryID;
    this.pathFromRoot=pathFromRoot;
    this.createDisplay();
    var self=this;
    this.index=index;
    this.display.onclick=function() {
        selector.setRootDirectoryID(rootDirectoryID);
        selector.setName(name);
        selector.setPathFromRoot(pathFromRoot);
        selector.play(self.getIndex());
    }
}

Maestro.VideoItem.prototype=new Maestro.MenuItem();
Maestro.VideoItem.constructor=Maestro.VideoItem;

Maestro.VideoItem.prototype.setIndex=function(index) {
    this.index=index;
}

Maestro.VideoItem.prototype.getIndex=function() {
    return this.index;
}

