Maestro.MainMenu=function() {
    this.menuManager=new Maestro.MenuItemManager();
    this.display=document.createElement("div");
    this.display.appendChild(this.menuManager.getDisplay());
}

Maestro.MainMenu.prototype.addMenuItem=function(item) {
    this.menuManager.addItem(item);
}

Maestro.MainMenu.prototype.enter=function() {
    this.menuManager.enter();
}

Maestro.MainMenu.prototype.getDisplay=function() {
    return this.display;
}
