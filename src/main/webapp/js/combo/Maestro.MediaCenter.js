Maestro.windowManager=new Maestro.WindowManager();
Maestro.windowManager.element=document.body;
Maestro.mainMenu=new Maestro.MainMenu();
Maestro.windowManager.addWindow(Maestro.mainMenu);
Maestro.windowManager.switchTo(Maestro.mainMenu);

Maestro.queryStringParameters = null;

Maestro.getQueryStringParameter =function (name) {
    if (Maestro.queryStringParameters == null) {
        Maestro.queryStringParameters = new Object();
        var params = window.location.search.substring(1).split('&');
        for (var i = 0; i < params.length; i++)
        {
            var param = params[i].split('=');
            Maestro.queryStringParameters[param[0]] = param[1];
        }
    }
    return Maestro.queryStringParameters[name];
} 