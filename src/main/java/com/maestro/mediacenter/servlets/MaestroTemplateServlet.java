package com.maestro.mediacenter.servlets;

import com.maestro.mediacenter.loaders.CssLoader;
import com.maestro.mediacenter.loaders.FileLoader;
import com.maestro.mediacenter.loaders.JavascriptLoader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;
import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Singleton
public class MaestroTemplateServlet extends HttpServlet {

    public MaestroTemplateServlet() {
        Logger.getLogger(this.getClass().getName()).info("Servlet Point");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String agent = request.getHeader("User-Agent");

        System.out.println("agent is "+agent);
        if(agent!=null&&agent.contains("CrKey")) {
            chromecastGet(request, response);
            return;
        }
        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        StringBuilder html = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(new FileLoader().getFile("templates/index.html")));

        for (String current = br.readLine(); current != null; current = br.readLine()) {
            html.append(current);
        }
        JavascriptLoader jsLoader = new JavascriptLoader();
        StringBuilder scriptsBuffer = new StringBuilder();
        for (String x : jsLoader.getScripts(JavascriptLoader.CLIENT_COMBO, request.getContextPath())) {
            File f = new File("." + x);
            long version = f.lastModified();
            scriptsBuffer.append("<script src=\"").append(x).append("?v=").append(version).append("\" ></script>");
        }
        String css = new CssLoader().getLinks(CssLoader.CLIENT_COMBO, request.getContextPath());
        String result = html.toString().replaceAll("\\{\\{scripts\\}\\}", scriptsBuffer.toString()).replaceAll("\\{\\{css\\}\\}", css).replaceAll("\\{\\{rootDirectory\\}\\}", request.getContextPath());
        response.getWriter().println(result);

    }
    
     protected void chromecastGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        StringBuilder html = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(new FileLoader().getFile("templates/chromecast.html")));

        for (String current = br.readLine(); current != null; current = br.readLine()) {
            html.append(current);
        }

        String result = html.toString();
        response.getWriter().println(result);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
