/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.files;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Al
 */
public class RootFolder extends Folder {

    public RootFolder(long rootDirectoryID, String name) {
        super(0, "", name);
        this.rootDirectoryID = rootDirectoryID;
    }

    public static String getLocation(long rootDirectoryID) {
        if (rootDirectoryID == 0) {
            return "";
        }
        return "\\\\192.168.0.210\\Volume_1\\TV Shows";
    }

    public static Collection<RootFolder> getRootFolders() {
        Collection<RootFolder> folders = new ArrayList<RootFolder>();
        folders.add(new RootFolder(1, "TV Shows"));
        return folders;
    }

    public static RootFolder getRootFolder(long rootDirectoryID) {
        return new RootFolder(1, "TV Shows");
    }
}
