/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.files;

/**
 *
 * @author Al
 */
public class File {

    protected String pathFromRoot;
    protected long rootDirectoryID;
    protected String name;

    public File(long rootDirectoryID, String pathFromRoot, String name) {
        this.pathFromRoot = pathFromRoot;
        this.rootDirectoryID = rootDirectoryID;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPathFromRoot() {
        return pathFromRoot;
    }

    public long getRootDirectoryID() {
        return rootDirectoryID;
    }
}
