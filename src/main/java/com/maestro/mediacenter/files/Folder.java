/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.files;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Al
 */
public class Folder extends File {

    protected Collection<Folder> folders;
    protected Collection<File> files;
    private Collection<String> extensions;

    public Folder(long rootDirectoryID, String pathFromRoot, String name) {

        super(rootDirectoryID, pathFromRoot, name);
        extensions = new ArrayList<String>();
        extensions.add("mp4");
        //java.io.File f = new java.io.File(location);
        //this.file=f;

        this.folders = new ArrayList<Folder>();
        this.files = new ArrayList<File>();
    }

    public void refresh() {
        this.folders = new ArrayList<Folder>();
        this.files = new ArrayList<File>();
        String folder = RootFolder.getLocation(this.rootDirectoryID);
        if (!pathFromRoot.equals("")) {
            folder += "/" + pathFromRoot;
        }
        folder += "/" + name;
        System.out.println(folder);
        java.io.File file = new java.io.File(folder);
        for (java.io.File f : file.listFiles()) {
            System.out.println(f);
            if (!f.isDirectory()) {
                String extension = Folder.getExtension(f);
                String name = this.getName();
                if (!pathFromRoot.equals("")) {
                    name = pathFromRoot + "/" + name;
                }
                if (extensions == null || extensions.contains(extension)) {
                    File currentFile = new File(rootDirectoryID, name, f.getName());
                    files.add(currentFile);
                }
            } else {
                String name = this.getName();
                if (!pathFromRoot.equals("")) {
                    name = pathFromRoot + "/" + name;
                }
                Folder currentFolder = new Folder(rootDirectoryID, name, f.getName());
                folders.add(currentFolder);
            }
        }
    }

    public static String getExtension(java.io.File f) {
        String name = f.getName();
        String extension = name.substring(name.lastIndexOf(".") + 1);
        return extension;

    }
}
