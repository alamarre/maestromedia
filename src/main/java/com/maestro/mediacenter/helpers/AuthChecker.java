package com.maestro.mediacenter.helpers;

import com.maestro.mediacenter.data.mappers.SessionMapper;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author al
 */
public class AuthChecker {
    
    @Inject
    CookieMonster cm;
    
    @Inject
    SessionMapper sm;
    
    @Inject
    NetworkWhitelist whitelist;
    
    public boolean isValid(HttpServletRequest request) {
        cm.setRequest(request);
        if(!whitelist.needsAuth(request.getRemoteAddr())) {
            return true;
        }
        sm.setCookieMonster(cm);
        return sm.isValidSession();
    }
}
