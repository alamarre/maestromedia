/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.helpers;

import com.maestro.mediacenter.data.mappers.NoAuthMapper;
import javax.inject.Inject;
import org.apache.commons.net.util.SubnetUtils;

/**
 *
 * @author al
 */
public class NetworkWhitelist {
    @Inject
    NoAuthMapper mapper;
    
    public boolean needsAuth(String ip) {
        for(String cidr: mapper.getNoAuthRanges()) {
            System.out.println("checking "+ip+" against "+cidr);
            
            SubnetUtils utils = new SubnetUtils(cidr);
            utils.setInclusiveHostCount(true);
            System.out.print(utils.getInfo().getLowAddress()+"-"+utils.getInfo().getHighAddress());
            if(utils.getInfo().isInRange(ip)) {
                System.out.println("in range");
                return false;
            }
        }
        
        return true;
    }
}
