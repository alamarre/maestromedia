/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.helpers;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Al
 */
public class PasswordHash {

    public static String getHash(String password, UUID uid) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            md.update(password.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();
            return new String(Base64.encodeBase64(sign(digest, uid.toString().getBytes())));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(PasswordHash.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(PasswordHash.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static byte[] sign(byte[] keyBytes, byte[] dataBytes) {
        try {
            Mac hmacSha256 = Mac.getInstance("HmacSHA256");
            SecretKeySpec key = new SecretKeySpec(keyBytes, "HmacSHA256");
            hmacSha256.init(key);
            byte[] b = hmacSha256.doFinal(dataBytes);

            return b;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
