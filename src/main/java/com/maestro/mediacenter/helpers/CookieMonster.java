/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.helpers;

import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Al
 */
@RequestScoped
public class CookieMonster {

    HttpServletRequest request;
    private Map<String, Cookie> cookies;

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public Cookie getCookie(String name) {
        if (cookies == null) {
            buildMap();
        }
        return cookies.get(name);
    }

    private void buildMap() {
        cookies = new HashMap<String, Cookie>();
        Cookie[] requestCookies=request.getCookies();
        if(requestCookies==null) {
            return;
        }
        for (Cookie c : requestCookies) {
            cookies.put(c.getName(), c);
        }
    }
}
