/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.data.models;

import java.util.UUID;

/**
 *
 * @author Al
 */
public class File implements Comparable {

    protected String pathFromRoot;
    protected UUID rootDirectoryID;
    protected String name;

    public File(UUID rootDirectoryID, String pathFromRoot, String name) {
        this.pathFromRoot = pathFromRoot;
        this.rootDirectoryID = rootDirectoryID;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPathFromRoot() {
        return pathFromRoot;
    }

    public UUID getRootDirectoryID() {
        return rootDirectoryID;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof File) {
            File f = (File)o;
            return name.compareTo(f.getName());
        }
        return -1;
    }
}
