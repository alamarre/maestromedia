/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.data.models;

import java.io.File;
import java.util.UUID;

/**
 *
 * @author Al
 */
public class RootFolder extends Folder {

    protected String location;

    public RootFolder() {
        this(UUID.randomUUID());
    }

    public RootFolder(UUID folderID) {
        super(folderID, "", "");
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
        File f = new File(location);
        this.setName(f.getName());
    }
}
