/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.data.models;

import com.maestro.mediacenter.data.mappers.RootFolderMapper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Al
 */
public class Folder extends File {

    protected List<Folder> folders;
    protected List<File> files;
    private transient Collection<String> extensions;

    public Folder(UUID rootDirectoryID, String pathFromRoot, String name) {

        super(rootDirectoryID, pathFromRoot, name);
        extensions = new ArrayList<String>();
        extensions.add("mp4");
        //java.io.File f = new java.io.File(location);
        //this.file=f;

        this.folders = new ArrayList<Folder>();
        this.files = new ArrayList<File>();
    }

    public void refresh(RootFolderMapper mapper) {
        this.folders = new ArrayList<Folder>();
        this.files = new ArrayList<File>();
        RootFolder fold = mapper.getFolder(this.getRootDirectoryID());
        String folder = fold.getLocation();
        if (!pathFromRoot.equals("")) {
            folder += "/" + pathFromRoot;
        }
        folder += "/" + name;
        System.out.println(folder);
        java.io.File file = new java.io.File(folder);
        for (java.io.File f : file.listFiles()) {
            System.out.println(f);
            if (!f.isDirectory()) {
                String extension = com.maestro.mediacenter.files.Folder.getExtension(f);
                String name = this.getName();
                if (!pathFromRoot.equals("")) {
                    name = pathFromRoot + "/" + name;
                }
                if (extensions == null || extensions.contains(extension)) {
                    File currentFile = new File(rootDirectoryID, name, f.getName());
                    files.add(currentFile);
                }
            } else {
                String name = this.getName();
                if (!pathFromRoot.equals("")) {
                    name = pathFromRoot + "/" + name;
                }
                Folder currentFolder = new Folder(rootDirectoryID, name, f.getName());
                folders.add(currentFolder);
            }
        }
        Collections.sort(this.files);
        Collections.sort(this.folders);
    }

    public static String getExtension(java.io.File f) {
        String name = f.getName();
        String extension = name.substring(name.lastIndexOf(".") + 1);
        return extension;

    }
}
