/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.data.mappers;

import com.maestro.data.abstraction.Datamaster;
import com.maestro.mediacenter.data.models.RootFolder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;

/**
 *
 * @author Al
 */
public class RootFolderMapper {

    //@Inject
    //CassandraDatabase db;
    
    @Inject
    Datamaster datamaster;

    public int save(RootFolder rootFolder) {
        //int result = db.execute("UPDATE root_folders SET location = ? WHERE KEY = ?", new Object[]{rootFolder.getLocation(), rootFolder.getRootDirectoryID()});
        //return result;
        datamaster.set("root_folders",rootFolder.getRootDirectoryID().toString(), rootFolder);
        return 0;
    }

    public Collection<RootFolder> getRootFolders() {
        Collection<RootFolder> list = new LinkedList<RootFolder>();
        /*CqlQuery query = cluster.getQuery();
         query.setQuery("SELECT * FROM RootFolders");
         QueryResult result = query.execute();
         CqlRows rows = (CqlRows)result.get();
         for (Iterator it = rows.iterator(); it.hasNext();) {
         Row row = (Row)it.next();
         ColumnSlice columnSlice = row.getColumnSlice();
         RootFolder folder=new RootFolder(UUID.fromString(columnSlice.getColumnByName("KEY").getValue().toString()));
         folder.setLocation(columnSlice.getColumnByName("location").getValue().toString());
         list.add(folder);
         }
         return list;*/
        /*StatementResult sr = db.executeQuery("SELECT * FROM root_folders");
        ResultSet results = sr.getResults();
        try {
            while (results.next()) {

                RootFolder rootFolder = this.map(results);
                list.add(rootFolder);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RootFolderMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;*/
        return datamaster.getRange("root_folders",null,RootFolder.class);
    }

    public RootFolder map(ResultSet result) {
        try {
            Object o = result.getObject("key");

            UUID uuid = (UUID) o;
            RootFolder rootFolder = new RootFolder(uuid);
            rootFolder.setLocation(result.getString("location"));
            return rootFolder;
        } catch (SQLException ex) {
            Logger.getLogger(RootFolderMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public RootFolder getFolder(UUID rootFolderID) {
        /*Logger.getLogger(RootFolderMapper.class.getName()).info("selecting via " + rootFolderID.toString());
        StatementResult sr = db.executeQuery("SELECT * FROM root_folders WHERE key=?", new Object[]{rootFolderID});
        ResultSet results = sr.getResults();
        try {
            if (results.next()) {

                RootFolder rootFolder = this.map(results);
                return rootFolder;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RootFolderMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;*/
        return datamaster.get("root_folders",rootFolderID.toString(),RootFolder.class);
    }
    
    public boolean delete(UUID folder) {
        //db.execute("DELETE FROM root_folders WHERE key=?", new Object[]{folder});
        datamaster.delete("root_folders",folder.toString());
        return true;
    }
}
