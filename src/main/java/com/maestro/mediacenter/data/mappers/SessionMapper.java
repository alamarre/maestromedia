/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.data.mappers;

import com.maestro.data.abstraction.Datamaster;
import com.maestro.mediacenter.helpers.CookieMonster;
import javax.inject.Inject;
import javax.servlet.http.Cookie;

/**
 *
 * @author Al
 */
public class SessionMapper {

    @Inject
    CookieMonster cookieMonster;
    
    @Inject
    Datamaster datamaster;

    public CookieMonster getCookieMonster() {
        return cookieMonster;
    }

    public void setCookieMonster(CookieMonster cookieMonster) {
        this.cookieMonster = cookieMonster;
    }

    public boolean isValidSession() {
        Cookie c = cookieMonster.getCookie("mc_session");
        if(c==null) {
            return false;
        }
        
        String result = datamaster.get("sessions", c.getValue(), String.class);
        return result!=null;
    }
}
