/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.data.mappers;

import com.maestro.data.abstraction.Datamaster;
import java.util.Collection;
import java.util.LinkedList;
import javax.inject.Inject;

public class NoAuthMapper {
      
    @Inject
    Datamaster datamaster;
    
    public Collection<String> getNoAuthRanges() {
        Collection<String> results = datamaster.getRange("no_auth_ips", null, String.class);
        
        if(results==null) {
            results = new LinkedList<String>();
            results.add("127.0.0.1/32");
            results.add("192.168.0.1/16");
        }
        
        return results;
    }
}
