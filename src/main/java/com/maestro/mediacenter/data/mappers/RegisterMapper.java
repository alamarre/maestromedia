package com.maestro.mediacenter.data.mappers;

import com.maestro.data.abstraction.Datamaster;
import com.maestro.mediacenter.data.models.User;
import com.maestro.mediacenter.helpers.PasswordHash;
import java.util.UUID;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Al
 */
public class RegisterMapper {

    @Inject
    Datamaster datamaster;

    public int register(String username, String password) {
        if (exists(username)) {
            return -1;
        }
        UUID userId = UUID.randomUUID();

        final String passwordHash = PasswordHash.getHash(password, userId);

        User user = new User();
        user.setUserId(username);
        user.setPasswordHash(passwordHash);
        user.setUsername(username);
        datamaster.set("users", username, user);
        return 1;
    }

    public boolean exists(String username) {
        return datamaster.get("users", username, User.class) != null;
    }

    public boolean login(String username, String password, HttpServletResponse response) {
        User user = datamaster.get("users", username, User.class);
        UUID uid = UUID.fromString(user.getUserId());
        String passwordHash = user.getPasswordHash();
        String testHash = PasswordHash.getHash(password, uid);
        System.out.println("password hashes " + testHash + " " + passwordHash);
        if (testHash.equals(passwordHash)) {
            UUID key = UUID.randomUUID();
            datamaster.set("sessions", key.toString(), user);
            Cookie c = new Cookie("mc_session", key.toString());
            response.addCookie(c);
            return true;
        }

        return false;
    }
}
