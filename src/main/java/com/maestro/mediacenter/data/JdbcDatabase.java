/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.data;

import java.sql.PreparedStatement;

/**
 *
 * @author Al
 */
public interface JdbcDatabase {

    public int execute(String query, Object[] objects);

    public StatementResult executeQuery(String query);

    public StatementResult executeQuery(String query, Object[] objects);

    public StatementResult executeQuery(String query, Object[] objects, boolean updating);

    public long insert(String query, Object[] objects);

    public void bindObject(PreparedStatement p, Object o, int index);
}
