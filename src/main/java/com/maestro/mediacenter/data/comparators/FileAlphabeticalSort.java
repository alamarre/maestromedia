package com.maestro.mediacenter.data.comparators;

import com.maestro.mediacenter.data.models.File;
import java.util.Comparator;


public class FileAlphabeticalSort implements Comparator<File> {

    @Override
    public int compare(File o1, File o2) {
        return o1.getName().compareTo(o2.getName());
    }

 


}
