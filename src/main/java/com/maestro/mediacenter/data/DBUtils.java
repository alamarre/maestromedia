package com.maestro.mediacenter.data;

import java.util.Collection;

public class DBUtils {

    public static String getPlaceHolders(Collection objects) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < objects.size(); i++) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append("?");
        }
        return sb.toString();
    }

    public static String getPlaceHolders(Object[] objects) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < objects.length; i++) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append("?");
        }
        return sb.toString();
    }

    public static Long getUnixTimestamp() {
        return new java.util.Date().getTime() / 1000;
    }
}
