/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Al
 */
public class StatementResult {

    private Connection connection;
    private ResultSet results;
    private PreparedStatement statement;
    private boolean closed = false;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public ResultSet getResults() {
        return results;
    }

    public void setResults(ResultSet results) {
        this.results = results;
    }

    public PreparedStatement getStatement() {
        return statement;
    }

    public void setStatement(PreparedStatement statement) {
        this.statement = statement;
    }

    public void close() {
        try {
            this.results.close();
        } catch (Exception e) {
        }
        try {
            this.statement.close();
        } catch (Exception e) {
        }
        try {
            this.connection.close();
        } catch (Exception e) {
        }
        closed = true;
    }

    public static boolean hasColumn(ResultSet results, String columnName) {
        try {
            results.findColumn(columnName);
            return true;
        } catch (SQLException sqlex) {
        }
        return false;
    }

    protected void finalize() {
        if (!closed) {
            this.close();
            Logger.getLogger(StatementResult.class.getName()).log(Level.SEVERE, null, "Class failed to clean up the DB after itself");
            Logger.getLogger(StatementResult.class.getName()).log(Level.SEVERE, null, new Throwable().getStackTrace());
        }
    }
}
