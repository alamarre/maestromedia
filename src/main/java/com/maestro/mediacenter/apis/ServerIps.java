/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.apis;

import com.google.gson.Gson;
import com.maestro.mediacenter.helpers.AuthChecker;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

@Path("/ServerIps")
@RequestScoped
public class ServerIps {
    
    @Inject
    AuthChecker ac;

    @GET
    public String handleGet(@Context HttpServletRequest request) {
        if (!ac.isValid(request)) {
            return APIResponse.getLoginResponse().serialize();
        }
        Gson g = new Gson();
        
        return g.toJson(new APIResponse("success", this.getAddresses()));

    }
    
    private Collection<String> getAddresses() {
        Collection<String> addresses = new LinkedList<String>();
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            
            while(networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                if(!networkInterface.isLoopback()) {
                    Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                    while(inetAddresses.hasMoreElements()) {
                        InetAddress address = inetAddresses.nextElement();
                        if(address instanceof Inet4Address) {
                            addresses.add(address.getHostAddress());
                        }
                    }
                }
            }
            
        } catch (SocketException ex) {
            Logger.getLogger(ServerIps.class.getName()).log(Level.SEVERE, null, ex);
        }
        return addresses;
    }
}
