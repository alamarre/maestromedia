/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.apis;

import com.maestro.mediacenter.data.mappers.RootFolderMapper;
import com.maestro.mediacenter.helpers.AuthChecker;
import com.maestro.mediacenter.helpers.Cors;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

/**
 *
 * @author Al
 */
@Path("videos")
@RequestScoped
public class VideoStreamApi {

    final int chunk_size = 1024 * 1024*1; // 1MB chunks
    @Inject
    RootFolderMapper mapper;
    
    @Inject
    AuthChecker ac;
    
    @Context 
    HttpServletRequest request;
    
    @Context 
    HttpServletResponse response;

    @GET
    @Produces("video/mp4")
    public Response getVideo(@HeaderParam("Range") String range, @QueryParam("rootDirectoryID") String rootDirectoryID, @QueryParam("pathFromRoot") String pathFromRoot, @QueryParam("name") String name, @Context HttpServletRequest request,@CookieParam(value = "rangeless-browser") String rangeless) {
        Cors.addResponseHeader(request, response);
        if (!ac.isValid(request)) {
            return null;
        }
        File fileToSend = new File(mapper.getFolder(UUID.fromString(rootDirectoryID)).getLocation() + "/" + pathFromRoot + "/" + name);
        try {
            return buildStream(fileToSend, range,rangeless);
        } catch (Exception ex) {
            Logger.getLogger(VideoStreamApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //from https://github.com/aruld/jersey-streaming
    private Response buildStream(final File asset, String range,String rangeless) throws Exception {
        // range not requested : Firefox, Opera, IE do not send range headers
        if (range == null) {
            StreamingOutput streamer = new StreamingOutput() {
                @Override
                public void write(final OutputStream output) throws IOException, WebApplicationException {

                    final FileChannel inputChannel = new FileInputStream(asset).getChannel();
                    final WritableByteChannel outputChannel = Channels.newChannel(output);
                    try {
                        inputChannel.transferTo(0, inputChannel.size(), outputChannel);
                    } finally {
                        // closing the channels
                        inputChannel.close();
                        outputChannel.close();
                    }
                }
            };
            NewCookie c = new NewCookie("rangeless-browser","true");
            
            return Response.ok(streamer).status(200).cookie(c).header(HttpHeaders.CONTENT_LENGTH, asset.length()).header("Accept-Ranges", "bytes").build();
           
            
//            range = "bytes=0-"+chunk_size;
        }

        String[] ranges = range.split("=")[1].split("-");
        final int from = Integer.parseInt(ranges[0]);
        /**
         * Chunk media if the range upper bound is unspecified. Chrome sends
         * "bytes=0-"
         */
        int to = chunk_size + from;
        if (to >= asset.length()) {
            to = (int) (asset.length() - 1);
        }
        if (ranges.length == 2) {
            to = Integer.parseInt(ranges[1]);
        }
        
        //Send full file for browsers that don't request range by default
        if(rangeless!=null&&rangeless.equals("true")) {
            to = (int) (asset.length() - 1);
        }
        
        EntityTag etag = new EntityTag(""+asset.getTotalSpace());

        final String responseRange = String.format("bytes %d-%d/%d", from, to, asset.length());
        final RandomAccessFile raf = new RandomAccessFile(asset, "r");
        raf.seek(from);

        final int len = to - from + 1;
        final MediaStreamer streamer = new MediaStreamer(len, raf);
        Response.ResponseBuilder res = Response.ok(streamer).status(206)
                .tag(etag)
                .header("Accept-Ranges", "bytes")
                .header("Content-Range", responseRange)
                .header(HttpHeaders.CONTENT_LENGTH, streamer.getLenth())
                .header(HttpHeaders.LAST_MODIFIED, new Date(asset.lastModified()));
        return res.build();
    }
}
