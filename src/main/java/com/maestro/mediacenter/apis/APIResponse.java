package com.maestro.mediacenter.apis;

import com.google.gson.Gson;
import java.net.HttpURLConnection;

public class APIResponse {

    private String result;
    private Object data; 
    //code should conform to HTTP status code
    private int code;

    public APIResponse(String result, Object data) {
        this(result,data,HttpURLConnection.HTTP_OK);
    }

    public APIResponse(String result, Object data, int code) {
        this.data = data;
        this.result = result;
        this.code = code;
    }
    
    public static APIResponse getLoginResponse() {
        return new APIResponse("error","must login",HttpURLConnection.HTTP_UNAUTHORIZED);
    }
    
    public String serialize() {
        Gson g = new Gson();
        return g.toJson(this);
    }
}
