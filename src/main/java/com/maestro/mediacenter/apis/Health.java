/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.apis;

import com.google.gson.Gson;
import com.maestro.mediacenter.helpers.AuthChecker;
import com.maestro.mediacenter.helpers.Cors;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

@Path("/Health")
@RequestScoped
public class Health {
    
    @Context 
    HttpServletRequest request;
    
    @Context 
    HttpServletResponse response;

    @GET
    public String handleGet(@Context HttpServletRequest request) {
        Cors.addResponseHeader(request, response);
        return "true";

    }
}
