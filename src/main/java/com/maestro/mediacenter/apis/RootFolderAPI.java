/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.apis;

import com.google.gson.Gson;
import com.maestro.mediacenter.data.mappers.RootFolderMapper;
import com.maestro.mediacenter.data.models.RootFolder;
import com.maestro.mediacenter.helpers.AuthChecker;
import com.maestro.mediacenter.helpers.Cors;
import java.util.UUID;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Al
 */
@Path("/RootFolders")
@RequestScoped
public class RootFolderAPI {

    @Inject
    RootFolderMapper mapper;
    
    @Inject
    AuthChecker ac;
    
    @Context 
    HttpServletRequest request;
    
    @Context 
    HttpServletResponse response;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String handle(@Context HttpServletRequest request) {
        Cors.addResponseHeader(request, response);
        if (!ac.isValid(request)) {
            return APIResponse.getLoginResponse().serialize();
        }
        Gson g = new Gson();
        return g.toJson(new APIResponse("success", mapper.getRootFolders()));
    }

    @POST
    public String create(@FormParam("location") String location, @Context HttpServletRequest request) {
        Cors.addResponseHeader(request, response);
        if (!ac.isValid(request)) {
            return APIResponse.getLoginResponse().serialize();
        }
        Gson g = new Gson();
        RootFolder rf = new RootFolder();
        rf.setLocation(location);
        int result = mapper.save(rf);
        if (result >= 0) {
            return g.toJson(new APIResponse("success", rf.getRootDirectoryID().toString()));
        } else {
            return g.toJson(new APIResponse("error", result));
        }
    }
    
    @DELETE
    public String delete(@QueryParam("folder") String folder) {
        Cors.addResponseHeader(request, response);
        Gson g = new Gson();
        return g.toJson(new APIResponse("success", mapper.delete(UUID.fromString(folder))));
    }
}
