/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.apis;

import com.google.gson.Gson;
import com.maestro.mediacenter.data.mappers.RootFolderMapper;
import com.maestro.mediacenter.data.models.Folder;
import com.maestro.mediacenter.helpers.AuthChecker;
import com.maestro.mediacenter.helpers.Cors;

import java.util.UUID;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Al
 */
@Path("/FolderList")
@RequestScoped
public class FolderListAPI {

    @Inject
    RootFolderMapper mapper;
    
    @Inject
    AuthChecker ac;
    
    @Context 
    HttpServletRequest request;
    
    @Context 
    HttpServletResponse response;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String test(@QueryParam("rootDirectoryID") String rootDirectory, @QueryParam("pathFromRoot") String pathFromRoot, @QueryParam("name") String name,@Context HttpServletRequest request) {
        if (!ac.isValid(request)) {
            return APIResponse.getLoginResponse().serialize();
        }
        Cors.addResponseHeader(request, response);
        Gson g = new Gson();

        UUID rootFolderID = UUID.fromString(rootDirectory);
        Folder folder = null;
        if (pathFromRoot == null) {
            folder = mapper.getFolder(rootFolderID);
        } else {
            folder = new Folder(rootFolderID, pathFromRoot, name);
        }
        folder.refresh(mapper);
        return g.toJson(new APIResponse("success", folder));


    }
}
