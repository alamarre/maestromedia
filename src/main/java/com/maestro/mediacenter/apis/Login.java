/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.apis;

import com.google.gson.Gson;
import com.maestro.mediacenter.data.mappers.RegisterMapper;
import com.maestro.mediacenter.helpers.Cors;
import com.maestro.mediacenter.loaders.FileLoader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("/Login")
@RequestScoped
public class Login {

    @Inject
    RegisterMapper mapper;
    
    @Context 
    HttpServletRequest request;
    
    @Context 
    HttpServletResponse response;
    
    @GET
    @Produces("text/html")
    public String handleGet() throws FileNotFoundException, IOException {
        Cors.addResponseHeader(request, response);
        BufferedReader br = new BufferedReader(new FileReader(new FileLoader().getFile("templates/login.html")));
        StringBuilder sb = new StringBuilder();
        String input;
        while((input=br.readLine())!=null) {
            sb.append(input);
        }
        return sb.toString();
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response handlePost(@FormParam("password") String password, @FormParam("username") String username, @Context HttpServletResponse response) throws FileNotFoundException, IOException, URISyntaxException {
        Cors.addResponseHeader(request, response);
        Gson g = new Gson();
        System.out.println("username is "+username);
        if(mapper.login(username, password, response)) {
            return Response.temporaryRedirect(new URI("../")).build();
        }
        return Response.ok(this.handleGet()).build();
    }
}
