/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.apis;

import com.google.gson.Gson;
import com.maestro.mediacenter.data.mappers.RegisterMapper;
import com.maestro.mediacenter.helpers.AuthChecker;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

@Path("/Register")
@RequestScoped
public class Register {

    @Inject
    RegisterMapper mapper;
    
    @Inject
    AuthChecker ac;

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public String handlePost(@FormParam("password") String password, @FormParam("username") String username,@Context HttpServletRequest request) {
        if (!ac.isValid(request)) {
            return APIResponse.getLoginResponse().serialize();
        }
        Gson g = new Gson();
        return g.toJson(new APIResponse("success", mapper.register(username, password)));

    }
}
