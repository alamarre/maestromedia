/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.loaders;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Logger;

public class FileLoader {

    public File getFile(String fileName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL packageUrl;

        packageUrl = classLoader.getResource("");
        Logger.getLogger(this.getClass().getName()).info(packageUrl.getPath());
        if (packageUrl.getProtocol().equals("jar")) {
            Logger.getLogger(this.getClass().getName()).info("using jar file");
            return getFileFromJar(fileName);
        }
        Logger.getLogger(this.getClass().getName()).info("using file system");
        Logger.getLogger(this.getClass().getName()).info(new File("").getAbsolutePath());
        return new File(packageUrl.getPath() + "/../../" + fileName);
    }

    private File getFileFromJar(String fileName) {
        Class cls = this.getClass();
        String className = cls.getName().concat(".class");
        Package pck = cls.getPackage();
        String packageName;
        if (pck != null) {
            packageName = pck.getName();
            if (className.startsWith(packageName)) {
                className = className.substring(packageName.length() + 1);
            }
        }
        java.net.URL url = cls.getResource(className);
        File f = new File(url.getPath());
        return new File(f.getParent() + "/../../../../" + fileName);
    }
}
