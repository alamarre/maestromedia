/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.mediacenter.loaders;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;

public class CssLoader {

    public static final int CLIENT_COMBO = 1;

    public CssLoader() {
    }

    public Collection<String> getScripts(int clientType, String rootFolder) {
        Collection<String> list = this.getScriptsInFolder("css", rootFolder);
        //list.addAll(this.getScriptsInFolder("js/combo"));
        return list;
    }

    private Collection<String> getScriptsInFolder(String folder, String rootFolder) {
        System.out.println(folder);
        ArrayList<String> list = new ArrayList<String>();
        File loadList = new FileLoader().getFile(folder + "/load.txt");
        if (loadList.isFile()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(new FileLoader().getFile(folder + "/load.txt")));
                for (String current = br.readLine(); current != null; current = br.readLine()) {
                    System.out.println(current);
                    if (current.endsWith(".css")) {
                        list.add(rootFolder + "/" + folder + "/" + current);
                    } else {
                        list.addAll(this.getScriptsInFolder(folder + "/" + current, rootFolder));
                    }
                }
            } catch (Exception e) {
            }
        } else {
            File fold = new FileLoader().getFile(folder);
            for (String current : fold.list()) {
                File f = new FileLoader().getFile(folder + "/" + current);
                if (f.isFile()) {
                    list.add(rootFolder + "/" + folder + "/" + current);
                } else {
                    list.addAll(this.getScriptsInFolder(folder + "/" + current, rootFolder));
                }
            }
        }
        return list;
    }

    public String getLinks(int clientType, String rootFolder) {
        StringBuilder sb = new StringBuilder();
        for (String file : this.getScripts(clientType, rootFolder)) {
            File f = new File("." + file);
            long version = f.lastModified();
            sb.append("<link rel=\"stylesheet\" href=\"").append(file).append("?v=").append(version).append("\" />");
        }
        return sb.toString();
    }
}
