package com.maestro.data.abstraction;

import java.util.HashMap;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Al
 */
@ApplicationScoped
public class CacheMap  {
    
    HashMap<String,Object> cache = new HashMap<String, Object>();
    
    public void put(String key, Object obj) {
        cache.put(key, obj);
    } 
    
    public boolean containsKey(String key) {
        return cache.containsKey(key);
    }
    
    public Object get(String key) {
        return cache.get(key);
    }
    
    public void remove(String key) {
        cache.remove(key);
    }
    
    
}
