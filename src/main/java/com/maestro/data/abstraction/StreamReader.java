package com.maestro.data.abstraction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StreamReader {
       
    public static String readAll(InputStream stream) {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        return readAll(br);
    }
    
    public static void readAllToBuilder(InputStream stream, StringBuilder sb) {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        readAllToStringBuilder(br,sb);
    }
    
    public static String readFile(String fileName) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        return readAll(br);
    }
    
    public static String readAll(BufferedReader br) {
        StringBuilder sb = new StringBuilder();
        try {
            
            String next = br.readLine();
            while(next!=null) {
                sb.append(next);
                sb.append("\n");
                next = br.readLine();
            }
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(StreamReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString();
    }
    
    public static void readAllToStringBuilder(BufferedReader br, StringBuilder sb) {

        try {
            
            String next = br.readLine();
            while(next!=null) {
                sb.append(next);
                sb.append("\n");
                next = br.readLine();
            }
            
        } catch (IOException ex) {
            Logger.getLogger(StreamReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
