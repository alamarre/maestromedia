package com.maestro.data.abstraction;

import com.maestro.mediacenter.loaders.FileLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;

public class FileDatamaster implements Datamaster {

    @Inject
    OmnySerializer serializer;
    @Inject
    CacheMap cacheMap;
    @Inject
    FileLoader fileLoader;
    String filePrefix = null;

    @Override
    public <T extends Object> T get(String section, String key, Class<T> type) {
        String name = section;

        if (key != null) {
            name += "/" + key;
        }
        String file = this.getFilename(name);
        try {
            return this.getValueFromFile(file, type);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileDatamaster.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void set(String section, String key, Object value) {
        String name = section;

        if (key != null) {
            name += "/" + key;
        }
        String file = this.getFilename(name);
        cacheMap.put(file, value);
        String result = serializer.serialize(value);
        try {
            File f = new File(file).getParentFile();

            if (!f.exists()) {
                f.mkdirs();
            }
            FileWriter fw = new FileWriter(file);
            fw.write(result);
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(FileDatamaster.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String escape(String input) {
        return input.replaceAll("\\", "::").replaceAll("/", "::");
    }

    @Override
    public <T extends Object> Collection<T> getRange(String section, String filter, Class<T> type) {
        String name = section;

        if (filter != null) {
            name += "/" + filter;
        }

        String folderName = this.getFolder(name);
        File folder = new File(folderName);
        if (folder.isDirectory()) {
            Collection<T> results = new LinkedList<T>();
            try {
                for (String file : folder.list()) {
                    String filePath = folder.getAbsolutePath() + "/" + file;
                    results.add(this.getValueFromFile(filePath, type));
                }
                return results;
            } catch (FileNotFoundException ex) {
            }
        }

        return null;
    }

    public <T extends Object> T getValueFromFile(String filename, Class<T> type) throws FileNotFoundException {
        if (cacheMap.containsKey(filename)) {
            return (T) cacheMap.get(filename);
        }
        String value = StreamReader.readFile(filename);
        T result = serializer.deserialize(value, type);
        cacheMap.put(filename, result);
        return result;
    }

    @Override
    public String joinParts(String... parts) {
        String result = "";
        for (String part : parts) {
            if (!result.isEmpty()) {
                result += "/";
            }
            result += this.escape(part);
        }
        return result;
    }

    @Override
    public void delete(String section, String key) {
        String name = section;

        if (key != null) {
            name += "/" + key;
        }
        final String filename = this.getFilename(name);

        File file = new File(filename);
        if (file.exists()) {
            file.delete();
        }
        cacheMap.remove(filename);
    }

    private String getFolder(String configName) {
        String suffix = configName;
        if (filePrefix == null) {
            File testFile = fileLoader.getFile("../../../config");
            System.out.println("checking for config directory in "+testFile.getAbsolutePath());
            if (testFile.exists()&&testFile.isDirectory()) {
                filePrefix = testFile.getAbsolutePath()+"/";
            } else {
                String os = System.getProperty("os.name");
                File f = new File(".");
                System.out.println("cwd is " + f.getAbsolutePath());
                if (os.contains("Windows")) {
                    filePrefix = System.getenv("SystemDrive") + "/omny/mediacenter/config/";
                } else {
                    filePrefix = "/etc/omny/mediacenter/config/";
                }
            }
            System.out.println("prefix is "+filePrefix);
        }
        String file = filePrefix + suffix;
        return file;
    }

    private String getFilename(String configName) {

        return this.getFolder(configName) + ".json";
    }
}
