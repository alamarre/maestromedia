package com.maestro.data.abstraction;

import com.google.gson.Gson;
import java.lang.reflect.Type;

public class OmnySerializer {
    
    
    public <T extends Object> T deserialize(String value, Class<T> type) {
        Gson gson = new Gson();
        if (type.equals(String.class)) {
            return (T)value;
        } else {
            return gson.fromJson(value, type);
        }
    }
    
    public String serialize(Object value) {
        Gson gson = new Gson();
        return gson.toJson(value);
    }
}
