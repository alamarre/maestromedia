package com.maestro.data.abstraction;

import java.util.Collection;

/**
 *
 * @author Al
 */
public interface Datamaster {

    void delete(String section, String key);

    <T extends Object> T get(String section, String key, Class<T> type);

    <T extends Object> Collection<T> getRange(String section, String filter, Class<T> type);

    String joinParts(String... parts);

    void set(String section, String key, Object value);
    
}
