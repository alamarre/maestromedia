Getting Started
---------------
Download the package for your system.

For Windows you can double click start.bat to run it until the computer is restart, or double click run-at-startup.bat to make it run at system startup.

Packages and simple instructions for other environments will be made available at a later date, but those with enough technical knowledge can build the project and run it with tomcat.

Once it is running then go to http://localhost in a browser and you should now see the start page for the media center.

Post-Execution Configuration
----------------------------

The first thing that needs to be done after launching is the registration of folders for the media center to use.

The interface has not yet been created so it has to be done via a manual AJAX request. Pasting the following into a JavaScript debugging console, which can be brought up by hitting F12, will allow you to make the request.

ajaxRequest({"url":"/api/RootFolders", type: "POST", data: { location: <location> }}); 

Where location is a string that would be a valid pathName to pass to a java.io.File constructor on the server's OS. Do not forget to escape all slashes. The network drive \\\\192.168.0.200\\example would be

ajaxRequest({"url":"/api/RootFolders", type: "POST", data: { location: '\\\\\\\\192.168.0.200\\\\example' }}); 


Network Access
--------------

The media center currently uses an IPV4 CIDR filter to determine whether the user needs to authenticate or not. 

It seemed unnecessary to require users on the same machine or local network to authenticate so 127.0.0.1/32 and 192.168.0.1/16 are permitted to use the system without authenticating.

Authenticated users or users in the local network can add users by calling the Register API from a Javascript console like so.

ajaxRequest({"url":"/api/Register", type: "POST", data: { username: <username>, password: <password> }});

Along with opening up a firewall this will allow users that aren't in the CIDR whitelist to authenticate and use the Media Center over the internet.
